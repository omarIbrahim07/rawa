//
//  ChatList.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 12/29/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class ChatList: Mappable {
    
    var id: Int?
    var toID: Int?
    var fromID: Int?
    var fromUserName: String?
    var fromUserImage: String?
    var toUserName: String?
    var toUserImage: String?
    var newID: String?

    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        toID <- map["to_id"]
        fromID <- map["from_id"]
        fromUserName <- map["fromuser_name"]
        fromUserImage <- map["fromuser_image"]
        toUserName <- map["touser_name"]
        toUserImage <- map["touser_image"]
        newID <- map["new_id"]
    }
    
}

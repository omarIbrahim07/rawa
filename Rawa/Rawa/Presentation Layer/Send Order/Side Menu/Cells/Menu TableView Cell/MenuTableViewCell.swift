//
//  MenuTableViewCell.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/12/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {

    @IBOutlet weak var menuImage: UIImageView!
    @IBOutlet weak var menuLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureCell()
        self.selectionStyle = .none
    }

    func configureCell() {
        menuImage.layer.cornerRadius = 8.0
    }
}

//
//  Order.swift
//  Rawa
//
//  Created by Omar Ibrahim on 4/20/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class Order: Mappable {
    
    var id: Int?
    var userId : String?
    var itemId : String?
    var quantity: Int?
//    var mosqueId: Int?
    var mosqueName : String?
//    var totalPrice : String?
    var cityId : String?
    var driverId: String?
    var status: String?
    var workerId: Int?
    var verifyImage: String?
    var longitude: String?
    var latitude: String?
    var orderType: Int?
    var createdAt: String?
    var updatedAt: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        userId <- map["user_id"]
        itemId <- map["item_id"]
        quantity <- map["quantity"]
//        mosqueId <- map["mosque_id"]
        mosqueName <- map["mosque_name"]
//        totalPrice <- map["total_price"]
        driverId <- map["driver_id"]
        status <- map["status"]
        verifyImage <- map["verify_image"]
        longitude <- map["longitude"]
        latitude <- map["latitude"]
        orderType <- map["order_type"]
        createdAt <- map["created_at"]
        updatedAt <- map["updated_at"]
    }
}

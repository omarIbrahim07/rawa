//
//  FollowingOrderCellViewModel.swift
//  Rawa
//
//  Created by Omar Ibrahim on 4/20/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

struct FollowingOrderCellViewModel {
    var id: Int?
    var userId: String?
    var itemId: String?
    var quantity: Int?
//    var mosqueId: Int?
    var mosqueName: String?
//    var totalPrice : String?
//    var cityId : String?
//    var driverId: String?
    var status: String?
    var verifyImage: String?
//    var longitude: String?
//    var latitude: String?
//    var orderType: Int?
//    var createdAt: String?
    var updatedAt: String?
}


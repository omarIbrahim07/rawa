//
//  HomeViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/10/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit
import SideMenu
import CoreLocation
import FSPagerView

var longitude: Double?
var latitude: Double?
var myChoiceAddress: String?

class HomeViewController: BaseViewController {
    
    lazy var viewModel: HomeViewModel = {
        return HomeViewModel()
    }()
    
    var error: APIError?
    var offers: [Offer] = [Offer]()
    
    var cities: [City] = [City]()
    var choosedCity: City?
    
    var items: [Item] = [Item]()
    var choosedItem: Item?
    var orderType: Int? = 1
    var mosjidID: Int?
    
    let locationManager = CLLocationManager()
    
    @IBOutlet weak var quantityView: UIView!
    @IBOutlet weak var packageView: UIView!
    @IBOutlet weak var masjidView: UIView!
    @IBOutlet weak var cityView: UIView!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var masjidLabel: UILabel!
    @IBOutlet weak var packageLabel: UILabel!
    @IBOutlet weak var quantityTextField: UITextField!
    @IBOutlet weak var firstView: UIView!
    @IBOutlet weak var secondView: UIView!
    @IBOutlet weak var firstViewBottleLabel: UILabel!
    @IBOutlet weak var firstViewShrinkTypeLabel: UILabel!
    @IBOutlet weak var firstViewPriceLabel: UILabel!
    @IBOutlet weak var secondViewBottleLabel: UILabel!
    @IBOutlet weak var secondViewShrinkTypeLabel: UILabel!
    @IBOutlet weak var secondViewPriceLabel: UILabel!
    @IBOutlet weak var payButton: UIButton!
    
    override func viewDidAppear(_ animated: Bool) {
            print(longitude)
            print(latitude)
            print(myChoiceAddress)
        if let address = myChoiceAddress {
            self.masjidLabel.textColor = #colorLiteral(red: 0.3882352941, green: 0.1568627451, blue: 0.4431372549, alpha: 1)
            self.masjidLabel.text = address
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setNavigationTitle()
        configureSideMenu()
        configureView()
        makeCustomLogoutBarButtonItem(imageName: "menu", buttonType: "left")
        makeNotificationIcon(imageName: "bell white", buttonType: "right")
        configureSideMenu()
        closeKeypad()
        
//        locationManager.requestWhenInUseAuthorization()
//        checkLocationManager()
        
        initVM()
    }
    
    func configureView() {
        cityView.layer.shadowColor = #colorLiteral(red: 0.4156862745, green: 0.007843137255, blue: 0.3803921569, alpha: 1)
        cityView.layer.shadowOpacity = 0.22
        cityView.layer.cornerRadius = 8.0
        cityView.layer.shadowRadius = 10

        masjidView.layer.cornerRadius = 8.0
        masjidView.layer.shadowColor = #colorLiteral(red: 0.4156862745, green: 0.007843137255, blue: 0.3803921569, alpha: 1)
        masjidView.layer.shadowOpacity = 0.22
        masjidView.layer.shadowRadius = 10
        
        packageView.layer.cornerRadius = 8.0
        packageView.layer.shadowColor = #colorLiteral(red: 0.4156862745, green: 0.007843137255, blue: 0.3803921569, alpha: 1)
        packageView.layer.shadowOpacity = 0.22
        packageView.layer.shadowRadius = 10

        quantityView.layer.cornerRadius = 8.0
        quantityView.layer.shadowColor = #colorLiteral(red: 0.4156862745, green: 0.007843137255, blue: 0.3803921569, alpha: 1)
        quantityView.layer.shadowOpacity = 0.22
        quantityView.layer.shadowRadius = 10

        
        payButton.layer.cornerRadius = 8.0
        payButton.layer.shadowColor = #colorLiteral(red: 0.4156862745, green: 0.007843137255, blue: 0.3803921569, alpha: 1)
        payButton.layer.shadowOpacity = 0.7
        
        firstView.addCornerRadius(raduis: 8.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        secondView.addCornerRadius(raduis: 8.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        cityLabel.textColor = #colorLiteral(red: 0.7411764706, green: 0.7960784314, blue: 0.8862745098, alpha: 1)
        masjidLabel.textColor = #colorLiteral(red: 0.7411764706, green: 0.7960784314, blue: 0.8862745098, alpha: 1)
        packageLabel.textColor = #colorLiteral(red: 0.7411764706, green: 0.7960784314, blue: 0.8862745098, alpha: 1)
        cityLabel.text = "city label".localized
        masjidLabel.text = "masjid label".localized
        packageLabel.text = "package label".localized
        quantityTextField.placeholder = "quantity".localized
        if "Lang".localized == "en" {
            quantityTextField.textAlignment = .left
        } else if "Lang".localized == "ar" {
            quantityTextField.textAlignment = .right
        }
        quantityTextField.text = ""
        payButton.setTitle("pay button label".localized, for: .normal)
    }

    
    func closeKeypad() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func viewTapped() {
        quantityTextField.endEditing(true)
    }
    
//    func checkLocationManager() {
//        if CLLocationManager.locationServicesEnabled() {
//            locationManager.delegate = self
//            locationManager.desiredAccuracy = kCLLocationAccuracyBest
//            locationManager.startUpdatingLocation()
//        }
//    }
    
    func initVM() {

        quantityTextField.delegate = self
        
        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.showError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }
        
        viewModel.updateSendingOrder = { [weak self] () in
            
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.sendingOrder {
                case .success:
                    self.showDonePopUp()
                case .failed:
                    print("Failed")
                }
            }
        }
        
        viewModel.reloadCitiesClosure = { [weak self] () in
            DispatchQueue.main.async { [weak self] () in
                self?.cities = self!.viewModel.getCities()
            }
        }
        
        viewModel.chooseCityClosure = { [weak self] () in
            
            DispatchQueue.main.async { [weak self] in
                self?.choosedCity = self?.viewModel.choosedCity
                if let city = self?.choosedCity {
                    self?.bindCity(city: city)
                }
            }
        }
        
        viewModel.reloadItemsClosure = { [weak self] () in
            DispatchQueue.main.async { [weak self] () in
                self?.items = self!.viewModel.getItems()
                if let items = self?.items {
                    self?.bindItemsDetails(items: items)
                }
            }
        }
        
        viewModel.chooseItemClosure = { [weak self] () in
            
            DispatchQueue.main.async { [weak self] in
                self?.choosedItem = self?.viewModel.choosedItem
                if let item = self?.choosedItem {
                    self?.bindItem(item: item)
                }
            }
        }
        
        viewModel.initFetch()
        viewModel.fetchItems()
//        viewModel.fetchOffers()
    }
    
    func bindItemsDetails(items: [Item]) {
        if "Lang".localized == "ar" {
            firstViewBottleLabel.text = "شرنك"
            secondViewBottleLabel.text = "شرنك"
            if let firstPrice: Int = items[0].price {
                firstViewPriceLabel.text = String(firstPrice) + " ريال"
            }
            if let secondPrice: Int = items[1].price {
                secondViewPriceLabel.text = String(secondPrice) + " ريال"
            }
        } else if "Lang".localized == "en" {
            firstViewBottleLabel.text = "Bottle"
            secondViewBottleLabel.text = "Bottle"
            if let firstPrice: Int = items[0].price {
                firstViewPriceLabel.text = String(firstPrice) + " SAR"
            }
            if let secondPrice: Int = items[1].price {
                secondViewPriceLabel.text = String(secondPrice) + " SAR"
            }
        }
        firstViewShrinkTypeLabel.text = items[0].title
        secondViewShrinkTypeLabel.text = items[1].title
    }
    
    // MARK:- Show Time Options Picker
    func setupCitiesSheet() {
        
        let sheet = UIAlertController(title: "city alert title".localized, message: "city alert message".localized, preferredStyle: .actionSheet)
        for city in cities {
            if "Lang".localized == "ar" {
                sheet.addAction(UIAlertAction(title: city.name, style: .default, handler: {_ in
                    self.viewModel.userPressed(city: city)
                }))
            } else if "Lang".localized == "en" {
                sheet.addAction(UIAlertAction(title: city.nameEn, style: .default, handler: {_ in
                    self.viewModel.userPressed(city: city)
                }))
            }
        }
        sheet.addAction(UIAlertAction(title: "cancel city".localized, style: .cancel, handler: nil))
        
//        sheet.popoverPresentationController?.sourceView = sortChoiceLabel
        
        self.present(sheet, animated: true, completion: nil)

    }
    
    func setupItemsSheet() {
        
        let sheet = UIAlertController(title: "item alert title".localized, message: "item alert message".localized, preferredStyle: .actionSheet)
        for item in items {
                sheet.addAction(UIAlertAction(title: item.title, style: .default, handler: {_ in
                    self.viewModel.itemPressed(item: item)
                }))
        }
        sheet.addAction(UIAlertAction(title: "cancel item".localized, style: .cancel, handler: nil))
        
//        sheet.popoverPresentationController?.sourceView = sortChoiceLabel
        
        self.present(sheet, animated: true, completion: nil)
    }


    
    func bindCity(city: City) {
        self.cityLabel.textColor = #colorLiteral(red: 0.3882352941, green: 0.1568627451, blue: 0.4431372549, alpha: 1)
        if "Lang".localized == "ar" {
            self.cityLabel.text = city.name
        } else if "Lang".localized == "en" {
            self.cityLabel.text = city.nameEn
        }
    }
    
    func showDonePopUp() {
        let alertController = UIAlertController(title: "تم إرسال الطلب بنجاح", message: "تهانينا", preferredStyle: .alert)
        //        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        //        alertController.addAction(cancelAction)
        let openAction = UIAlertAction(title: "تم", style: .default) { (action) in
//            self.goToHomePage()
            self.viewDidLoad()
        }
        alertController.addAction(openAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func goToHomePage() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "HomeNavigationVC")
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }
        
    func bindItem(item: Item) {
        self.packageLabel.textColor = #colorLiteral(red: 0.3882352941, green: 0.1568627451, blue: 0.4431372549, alpha: 1)
        self.packageLabel.text = item.title
    }
    
    func setNavigationTitle() {
        navigationItem.title = "making order title".localized
    }
                
    func makeCustomLogoutBarButtonItem(imageName: String, buttonType: String) {
        
        let barButton = UIButton()
        
        let containerButton = UIButton(frame: CGRect(x: -3, y: 0, width: 28, height: 28))
        
        containerButton.setImage(UIImage(named: imageName), for: .normal)
        
        containerButton.setImage(UIImage(named: imageName), for: .highlighted)
        
        containerButton.imageView?.contentMode = .scaleAspectFit
        
        if "Lang".localized == "en" {
            containerButton.addTarget(self, action: #selector(self.configureLeftSideMenu), for: UIControl.Event.touchUpInside)
        } else if "Lang".localized == "ar" {
            containerButton.addTarget(self, action: #selector(self.configureRightSideMenu), for: UIControl.Event.touchUpInside)
        }
        
        barButton.addSubview(containerButton)
        
        if buttonType == "right" {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: barButton)
        } else if buttonType == "left" {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: barButton)
        }
    }
    
    func makeNotificationIcon(imageName: String, buttonType: String) {
        
        let barButton = UIButton()
        
        let containerButton = UIButton(frame: CGRect(x: +3, y: 0, width: 32, height: 32))
        
        containerButton.setImage(UIImage(named: imageName), for: .normal)
        
        containerButton.setImage(UIImage(named: imageName), for: .highlighted)
        
        containerButton.imageView?.contentMode = .scaleAspectFit
        
        containerButton.addTarget(self, action: #selector(self.goToNotifications), for: UIControl.Event.touchUpInside)
        
        barButton.addSubview(containerButton)
        
        if buttonType == "right" {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: barButton)
        } else if buttonType == "left" {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: barButton)
        }
    }
    
    func saveLocation(longitude: Double, latitude: Double) {
        UserDefaults.standard.set(longitude, forKey: "longitude") //Bool
        UserDefaults.standard.set(latitude, forKey: "latitude") //Bool
    }
    
    func sendOrder() {
        var long: String?
        var lat: String?
        var masjID: Int?
                
        guard let userID: Int = UserDefaultManager.shared.currentUser?.id, userID > 0 else {
            let apiError = APIError()
            apiError.message = "عذرًا أنت غير مسجل لدينا"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }

        guard let cityID: Int = choosedCity?.id, cityID > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال المدينة"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let orderType: Int = self.orderType, orderType >= 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال نوع التوصيل"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        if orderType == 0 {
            guard let masjidID: Int = self.mosjidID, masjidID > 0 else {
                let apiError = APIError()
                apiError.message = "برجاء إدخال المسجد المراد التوصيل له"
                self.viewModel.error = apiError
                self.viewModel.state = .error
                return
            }
            masjID = masjidID
        } else if orderType == 1 {
            guard let longitude: Double = longitude, longitude > 0 else {
                let apiError = APIError()
                apiError.message = "يرجى إدخال العنوان"
                self.viewModel.error = apiError
                self.viewModel.state = .error
                return
            }

            guard let latitude: Double = latitude, latitude > 0 else {
                let apiError = APIError()
                apiError.message = "يرجى إدخال العنوان"
                self.viewModel.error = apiError
                self.viewModel.state = .error
                return
            }
            long = String(longitude)
            lat = String(latitude)
        }
        
        guard let mosqueName: String = self.masjidLabel.text, mosqueName != "masjid label".localized else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال المسجد"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }

        
        guard let itemID: Int = self.choosedItem?.id, itemID > 0 else {
            let apiError = APIError()
            apiError.message = "من فضلك اختر نوع المياه المراد توصيلها"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }

        guard let quantity: String = quantityTextField.text, quantity.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال الكمية"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        let quant = Int(quantity)
        
        if orderType == 0 {
            self.viewModel.sendOrder(userID: userID, itemId: itemID, quantity: quant!, mosqueId: masjID!, mosqueName: mosqueName, cityId: cityID, orderType: orderType, longitude: "", latitude: "")
        } else if orderType == 1 {
            self.viewModel.sendOrder(userID: userID, itemId: itemID, quantity: quant!, mosqueId: 0, mosqueName: mosqueName, cityId: cityID, orderType: orderType, longitude: long!, latitude: lat!)
        }
                        
    }
    
    @objc func configureSideMenu() {
        // Define the menus
        let leftMenuNavigationControllerr = storyboard?.instantiateViewController(withIdentifier: "LefttSideMenu") as? UISideMenuNavigationController

        SideMenuManager.default.leftMenuNavigationController?.menuWidth = view.frame.size.width - 100
        SideMenuManager.default.leftMenuNavigationController = leftMenuNavigationControllerr
        
        if let x = self.navigationController
        {
            
            SideMenuManager.default.addPanGestureToPresent(toView: x.navigationBar)
            SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: x.view, forMenu: SideMenuManager.PresentDirection.left)
        }
        
        leftMenuNavigationControllerr!.statusBarEndAlpha = 0
    }
    
    @objc func configureLeftSideMenu() {
        // Define the menus
        let menu = storyboard!.instantiateViewController(withIdentifier: "LefttSideMenu") as! UISideMenuNavigationController
        SideMenuManager.default.rightMenuNavigationController?.leftSide = true
        SideMenuManager.default.leftMenuNavigationController = menu

        present(menu, animated: true, completion: nil)
        
        // (Optional) Prevent status bar area from turning black when menu appears:
        menu.statusBarEndAlpha = 0
    }
    
    @objc func configureRightSideMenu() {
        // Define the menus
        let menu = storyboard!.instantiateViewController(withIdentifier: "LefttSideMenu") as! UISideMenuNavigationController
        SideMenuManager.default.leftMenuNavigationController?.leftSide = false
        SideMenuManager.default.rightMenuNavigationController = menu
        
        present(menu, animated: true, completion: nil)
        
        // (Optional) Prevent status bar area from turning black when menu appears:
        menu.statusBarEndAlpha = 0
    }
    
    // MARK:- Actions
    @IBAction func cityButtonIsPressed(_ sender: Any) {
        print("City Button Pressed")
        setupCitiesSheet()
    }
    
    @IBAction func packageButtonIsPressed(_ sender: Any) {
        setupItemsSheet()
    }
    
    @IBAction func payButtonIsPressed(_ sender: Any) {
        self.sendOrder()
    }
    
    @IBAction func masjidButtonIsPressed(_ sender: Any) {
        self.goToMap()
    }
    
    // MARK:- Navigation
    @objc func goToNotifications() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "FollowingOrdersViewController") as! FollowingOrdersViewController
        //        viewController.typeIdSelected = self.typeIdSelected
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToMap() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        //        viewController.typeIdSelected = self.typeIdSelected
        navigationController?.pushViewController(viewController, animated: true)
    }
    
}

extension HomeViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //For mobile numer validation
        if textField == quantityTextField {
            let allowedCharacters = CharacterSet(charactersIn:"0123456789")//Here change this characters based on your requirement
            let characterSet = CharacterSet(charactersIn: string)
            return allowedCharacters.isSuperset(of: characterSet)
        }
        return true
    }
}

//// MARK:- Location Manager Delegate
//extension HomeViewController: CLLocationManagerDelegate {
//    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
////        UserDefaults.standard.set(, forKey: "ID") //Bool
//        if let location = locations.first {
//            print(location.coordinate)
//            saveLocation(longitude: location.coordinate.longitude, latitude: location.coordinate.latitude)
//        }
//    }
//}

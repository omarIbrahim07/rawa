//
//  SideMenuViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/10/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit
import MOLH

class SideMenuViewController: UIViewController {
    
    let menuArray: [String] = [
        "Main",
        "Share App",
        "About app",
        "Following Orders",
"العربية",
"Logout"
    ]
    
    let arabicMenuArray: [String] = [
        "الرئيسية",
"مشاركة التطبيق",
"من نحن",
"متابعة الطلبات",
"English",
"تسجيل الخروج"
    ]
        
    let menuImages: [String] = [
        "home copy",
        "share copy",
        "Android Mobile – 1 copy",
        "notifi",
        "language",
        "logout"
    ]


    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        configureMenuTableViewCell()
    }
    
    func configureView() {
        if "Lang".localized == "en" {
            tableView.roundCorners([.topRight], radius: 40)
        } else if "Lang".localized == "ar" {
            tableView.roundCorners([.topLeft], radius: 40)
        }
    }
    
    func configureMenuTableViewCell() {
        tableView.register(UINib(nibName: "MenuTableViewCell", bundle: nil), forCellReuseIdentifier: "MenuTableViewCell")
        tableView.register(UINib(nibName: "InfoTableViewCell", bundle: nil), forCellReuseIdentifier: "InfoTableViewCell")
        tableView.register(UINib(nibName: "LogoutTableViewCell", bundle: nil), forCellReuseIdentifier: "LogoutTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
    }
    
//    func goToMentainanceOrders() {
//        if let mentainanceOrdersController = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "MentainanceOrdersViewController") as? MentainanceOrdersViewController {
//            //                rootViewContoller.test = "test String"
//            self.present(mentainanceOrdersController, animated: true, completion: nil)
//            print("Mentainance Orders")
//        }
//    }
//    
//    func goToOffers() {
//        if let offersController = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "OffersViewController") as? OffersViewController {
//            //                rootViewContoller.test = "test String"
////            offersController.choosedServiceEnum = .menu
//            offersController.viewModel.choosedServiceEnum = .menu
//            self.present(offersController, animated: true, completion: nil)
//            print("Offers")
//        }
//    }
//    
//    func goToBids() {
////        if let myAccountNavigationController = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "BidsViewControllernav") as? UINavigationController, let rootViewContoller = myAccountNavigationController.viewControllers[0] as? BidsViewController {
////            //                rootViewContoller.test = "test String"
////            self.present(myAccountNavigationController, animated: true, completion: nil)
////            print("Fes")
////        }
//        
//        if let bidsController = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "BidsViewController") as? BidsViewController {
//            //                rootViewContoller.test = "test String"
//            self.present(bidsController, animated: true, completion: nil)
//            print("Bids")
//        }
//    }
//    
//    func goToListOfChats() {
//        if let listOfChats = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "ListOfChatsViewController") as? ListOfChatsViewController {
//            //                rootViewContoller.test = "test String"
//            self.present(listOfChats, animated: true, completion: nil)
//            print("List Of Chats")
//        }
//    }
//    
//    func goToMyAccount() {
//        if let myAccount = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "MyAccountViewController") as? MyAccountViewController {
//            //                rootViewContoller.test = "test String"
//            self.present(myAccount, animated: true, completion: nil)
//            print("My Account")
//        }
//    }
//    
//    func goToSafteyProcedures() {
//        if let safteyProceduresVC = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "SafteyProceduresViewController") as? SafteyProceduresViewController {
//            //                rootViewContoller.test = "test String"
//            self.present(safteyProceduresVC, animated: true, completion: nil)
//            print("Saftey Procedures")
//        }
//    }
//    
//    func goToTermsAndConditions() {
//        if let termsAndConditionsVC = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "TermsAndConditionsViewController") as? TermsAndConditionsViewController {
//            //                rootViewContoller.test = "test String"
//            self.present(termsAndConditionsVC, animated: true, completion: nil)
//            print("Terms and conditions")
//        }
//    }
//    
//    func goToPromoCode() {
//        if let promoCodeVC = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "PromoCodeViewController") as? PromoCodeViewController {
//            //                rootViewContoller.test = "test String"
//            self.present(promoCodeVC, animated: true, completion: nil)
//            print("Terms and conditions")
//        }
//    }
//    
//    func goToComplainsAndSuggestions() {
//        if let complainsAndSuggstionsVC = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "ComplainsAndSuggestionsViewController") as? ComplainsAndSuggestionsViewController {
//            //                rootViewContoller.test = "test String"
//            self.present(complainsAndSuggstionsVC, animated: true, completion: nil)
//            print("Terms and conditions")
//        }
//    }
//    
//    func goToConnectWithUs() {
//        if let complainsAndSuggstionsVC = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "ContactWithUsViewController") as? ContactWithUsViewController {
//            //                rootViewContoller.test = "test String"
//            self.present(complainsAndSuggstionsVC, animated: true, completion: nil)
//            print("Connect With Us")
//        }
//    }
//    
//    func goToAboutApp() {
//        if let complainsAndSuggstionsVC = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "AboutAppViewController") as? AboutAppViewController {
//            //                rootViewContoller.test = "test String"
//            self.present(complainsAndSuggstionsVC, animated: true, completion: nil)
//            print("About App")
//        }
//    }
//    
    func logout() {
                
        AuthenticationAPIManager().logout(basicDictionary: [:], onSuccess: { (message) -> String in
            
            self.goToLogIn()
            
            return "70bby"
            
        }) { (error) in
            print(error)
        }
    }
    
    func goToLogIn() {
        if let signInNavigationController = storyboard?.instantiateViewController(withIdentifier: "LoginNavigationVC") as? UINavigationController, let loginViewController = signInNavigationController.viewControllers[0] as? LoginViewController {

            UIApplication.shared.keyWindow?.rootViewController = signInNavigationController
        }
    }

//    func goToImageDetails(imageURL: String) {
//        if let imageDetailsVC = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "ImageZoomedViewController") as? ImageZoomedViewController {
//            //                rootViewContoller.test = "test String"
//            imageDetailsVC.imageURL = imageURL
//            self.present(imageDetailsVC, animated: true, completion: nil)
//            print("bidDetailsVC")
//        }
//    }
    
    func goToAboutUs() {
        if let myAccountNavigationController = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "AboutUsNavVC") as? UINavigationController, let rootViewContoller = myAccountNavigationController.viewControllers[0] as? AboutUsViewController {
            //                rootViewContoller.test = "test String"
            self.present(myAccountNavigationController, animated: true, completion: nil)
            print("Fes")
        }
    }
    
    func changeLanguage() {
        MOLH.setLanguageTo(MOLHLanguage.currentAppleLanguage() == "en" ? "ar" : "en")
        MOLH.reset()
        reset()
//        languageLabel.text = "Language".localized
    }
    
    func reset() {
        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        let stry = UIStoryboard(name: "Main", bundle: nil)
        rootviewcontroller.rootViewController = stry.instantiateViewController(withIdentifier: "HomeNavigationVC")
    }
    
    func goToNotifications() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "FollowingOrdersViewController") as! FollowingOrdersViewController
        //        viewController.typeIdSelected = self.typeIdSelected
        navigationController?.pushViewController(viewController, animated: true)
    }


}

extension SideMenuViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuArray.count + 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            if let cell: InfoTableViewCell = tableView.dequeueReusableCell(withIdentifier: "InfoTableViewCell", for: indexPath) as? InfoTableViewCell {
                cell.delegate = self
                if let user = UserDefaultManager.shared.currentUser {
//                    cell.emailLabel.text = user.email
                    if let firstName: String = user.firstName, let lastName: String = user.lastName {
//                        cell.userNameLabel.text = firstName + " " + lastName
                    }
                    
                    if let image: String = user.image {
                        cell.userImage.loadImageFromUrl(imageUrl: ImageURL_USERS + image)
                    }
                    
                    if let email: String = user.email {
//                        cell.emailLabel.text = email
                    }
                }
                return cell
            }
        }
        
        else if indexPath.row > 0 && indexPath.row < 7 {
            if let cell: MenuTableViewCell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell", for: indexPath) as? MenuTableViewCell {
                
                
                if "Lang".localized == "en" {
                    cell.menuLabel.text = menuArray[indexPath.row - 1]
                } else if "Lang".localized == "ar" {
                    cell.menuLabel.text = arabicMenuArray[indexPath.row - 1]
                }
                cell.menuImage.image = UIImage(named: menuImages[indexPath.row - 1])
                return cell
            }
        }
        
//        else if indexPath.row == 6 {
//            if let cell: LogoutTableViewCell = tableView.dequeueReusableCell(withIdentifier: "LogoutTableViewCell", for: indexPath) as? LogoutTableViewCell {
//                if "Lang".localized == "en" {
//                    cell.logoutLabel.text = "logout".localized
//                } else if "Lang".localized == "ar" {
//                    cell.logoutLabel.text = "logout".localized
//                }
//                return cell
//            }
//        }
        
        return UITableViewCell()
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 1 {
            dismiss(animated: true, completion: nil)
        }
        else if indexPath.row == 2 {
        } else if indexPath.row == 3 {
            goToAboutUs()
        } else if indexPath.row == 4 {
            goToNotifications()
        } else if indexPath.row == 5 {
            changeLanguage()
        } else if indexPath.row == 6 {
            self.logout()
//            goToMyAccount()
        } else if indexPath.row == 7 {
//            goToSafteyProcedures()
        } else if indexPath.row == 8 {
            
        } else if indexPath.row == 9 {
//            goToTermsAndConditions()
        } else if indexPath.row == 10 {
//            goToPromoCode()
        } else if indexPath.row == 11 {
//            goToComplainsAndSuggestions()
        } else if indexPath.row == 12 {
//            goToConnectWithUs()
        } else if indexPath.row == 13 {
//            goToAboutApp()
        } else if indexPath.row == 14 {
//            self.logout()
        }

        
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension SideMenuViewController: profileImageTableViewCellDelegate {
    func profileImageButtonPressed() {
        if let image = UserDefaultManager.shared.currentUser?.image {
//            self.goToImageDetails(imageURL: ImageURL_USERS + image)
        }
    }
}


//
//  ItemViewModel.swift
//  Rawa
//
//  Created by Omar Ibrahim on 4/9/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

struct ItemViewModel {
    let id: Int
    let type: String
}

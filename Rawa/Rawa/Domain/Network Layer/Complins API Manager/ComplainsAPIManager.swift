//
//  ComplainsAPIManager.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/28/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class ComplainsAPIManager: BaseAPIManager {

    func sendFeedback(basicDictionary params:APIParams , onSuccess: @escaping (Bool)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: SEND_FEEDBACK_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let saved: Bool = response["saved"] as? Bool {
                
                onSuccess(saved)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
}

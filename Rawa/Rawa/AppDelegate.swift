//
//  AppDelegate.swift
//  Rawa
//
//  Created by Omar Ibrahim on 4/6/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import MOLH
import GoogleMaps
import GooglePlaces

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

//MARK:- Helpers
   func checkedForLoggedInUser() {
       if let _ = UserDefaultManager.shared.currentUser, let _ = UserDefaultManager.shared.authorization {
           let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
           let viewController = storyboard.instantiateViewController(withIdentifier: "HomeNavigationVC")
           window!.rootViewController = viewController
           window!.makeKeyAndVisible()
       }
   }
    
    func reset() {
        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        let stry = UIStoryboard(name: "Main", bundle: nil)
        rootviewcontroller.rootViewController = stry.instantiateViewController(withIdentifier: "HomeNavigationVC")
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        print("7ossa DidFinishLaunch")
        MOLH.shared.activate(true)
        
        GMSServices.provideAPIKey(googleApiKey)
        GMSPlacesClient.provideAPIKey(googleApiKey)

        checkedForLoggedInUser()

//        FirebaseApp.configure()
//        getFirebaseToken()
//        GMSServices.provideAPIKey(googleApiKey)
//
//
//        // [START set_messaging_delegate]
//        Messaging.messaging().delegate = self
//        // [END set_messaging_delegate]
//        // Register for remote notifications. This shows a permission dialog on first run, to
//        // show the dialog at a more appropriate time move this registration accordingly.
//        // [START register_for_notifications]
//        if #available(iOS 10.0, *) {
//          // For iOS 10 display notification (sent via APNS)
//          UNUserNotificationCenter.current().delegate = self
//
//          let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
//          UNUserNotificationCenter.current().requestAuthorization(
//            options: authOptions,
//            completionHandler: {_, _ in })
//        } else {
//          let settings: UIUserNotificationSettings =
//          UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
//          application.registerUserNotificationSettings(settings)
//        }
//
//        application.registerForRemoteNotifications()

        // [END register_for_notifications]
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        print("7ossa applicationWillResignActive")
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        print("7ossa applicationDidEnterBackground")
        
//        if let user = UserDefaultManager.shared.currentUser {
//            if let userID: Int = user.id {
//                // Access Firebase database
//                let databaseRefrence = Database.database().reference()
//
//                // create room by auto generated id
//                let stringUserID = String(userID)
//                let room = databaseRefrence.child("online").child(stringUserID).child("state")
//
//                // Add data in created room
//                let dataArray: [String : Any] = ["state" : "0"]
//
//                // Set value with completion block
//                room.setValue(dataArray) { (error, ref) in
//                    if error == nil {
//                        print("Offline")
//                    } else {
//                        print("5betha")
//                    }
//                }
//
//            }
//        }

        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        print("7ossa applicationWillEnterForeground")
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        print("7ossa applicationDidBecomeActive")
        
//        if let user = UserDefaultManager.shared.currentUser {
//            if let userID: Int = user.id {
//                // Access Firebase database
//                let databaseRefrence = Database.database().reference()
//
//                // create room by auto generated id
//                let stringUserID = String(userID)
//                let room = databaseRefrence.child("online").child(stringUserID).child("state")
//
//                // Add data in created room
//                let dataArray: [String : Any] = ["state" : "1"]
//
//                // Set value with completion block
//                room.setValue(dataArray) { (error, ref) in
//                    if error == nil {
//                        print("ONLINE")
//                    } else {
//                        print("5betha")
//                    }
//                }
//
//            }
//        }
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        print("7ossa applicationWillTerminate")
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}


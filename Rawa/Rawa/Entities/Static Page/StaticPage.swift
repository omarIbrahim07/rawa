//
//  StaticPage.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/18/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class StaticPage: Mappable {
    
    var id: Int?
    var name: String?
    var content: String?
    var createdAt: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        content <- map["content"]
        createdAt <- map["created_at"]
    }
    
}

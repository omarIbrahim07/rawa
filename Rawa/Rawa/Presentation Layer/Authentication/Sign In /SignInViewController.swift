//
//  SignInViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 8/26/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit

class SignInViewController: BaseViewController {
    
    lazy var viewModel: SignInViewModel = {
        return SignInViewModel()
    }()
    
    var checked: Bool = false
    var error: APIError?
    var user: UserSigned?
    
    var cities: [City] = [City]()
    var choosedCity: City?
    var genders: [GenderViewModel] = [GenderViewModel]()
    var choosedGender: GenderViewModel?
    
    @IBOutlet weak var logoView: UIView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var confirmPasswordView: UIView!

    
    override func viewDidLoad() {
                
        super.viewDidLoad()
        configureView()
        closeKeypad()
        
        // Do any additional setup after loading the view.
        initVM()
        viewModel.initFetch(locationID: 1)
    }
    
    // MARK:- Init View Model
    func initVM() {
        
        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.showError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }
        
        viewModel.updateUser = { [weak self] () in
            DispatchQueue.main.async { [weak self] () in
                self?.user = self!.viewModel.getUser()
                if let user: UserSigned = self?.user {
                    if let userId: Int = user.id {
                        self?.showRegistrationSuccessPopUp()
//                        self!.goToVerifyPhone(id: userId)
                    }
                }
            }
        }
                    
    }
    
    func showRegistrationSuccessPopUp() {
        let alertController = UIAlertController(title: "signup title popup".localized, message: "signup message popup".localized, preferredStyle: .alert)
        let openAction = UIAlertAction(title: "signup okay button".localized, style: .default) { (action) in
            self.goToLogIn()
        }
        alertController.addAction(openAction)
        self.present(alertController, animated: true, completion: nil)
    }

        
    func configureView() {
        
        nameView.layer.shadowColor = #colorLiteral(red: 0.4156862745, green: 0.007843137255, blue: 0.3803921569, alpha: 1)
        nameView.layer.shadowOpacity = 0.22
        nameView.layer.cornerRadius = 8.0
        nameView.layer.shadowRadius = 10

        phoneView.layer.shadowColor = #colorLiteral(red: 0.4156862745, green: 0.007843137255, blue: 0.3803921569, alpha: 1)
        phoneView.layer.shadowOpacity = 0.22
        phoneView.layer.cornerRadius = 8.0
        phoneView.layer.shadowRadius = 10

        passwordView.layer.cornerRadius = 8.0
        passwordView.layer.shadowColor = #colorLiteral(red: 0.4156862745, green: 0.007843137255, blue: 0.3803921569, alpha: 1)
        passwordView.layer.shadowOpacity = 0.22
        passwordView.layer.shadowRadius = 10
        
        confirmPasswordView.layer.cornerRadius = 8.0
        confirmPasswordView.layer.shadowColor = #colorLiteral(red: 0.4156862745, green: 0.007843137255, blue: 0.3803921569, alpha: 1)
        confirmPasswordView.layer.shadowOpacity = 0.22
        confirmPasswordView.layer.shadowRadius = 10
        
        signUpButton.layer.cornerRadius = 8.0
        signUpButton.layer.shadowColor = #colorLiteral(red: 0.4156862745, green: 0.007843137255, blue: 0.3803921569, alpha: 1)
        signUpButton.layer.shadowOpacity = 0.7
        
        nameTextField.placeholder = "name".localized
        phoneTextField.placeholder = "phone".localized
        passwordTextField.placeholder = "password".localized
        confirmPasswordTextField.placeholder = "confirm".localized
        signUpButton.setTitle("sign up".localized, for: .normal)
        
        
        self.navigationItem.title = "sign up".localized
        logoView.roundCorners([.bottomLeft, .bottomRight], radius: 30.0)
//        signUpButton.addCornerRadius(raduis: 5.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        if "Lang".localized == "en" {
            nameTextField.textAlignment = .left
            confirmPasswordTextField.textAlignment = .left
            phoneTextField.textAlignment = .left
            passwordTextField.textAlignment = .left
        } else if "Lang".localized == "ar" {
            phoneTextField.textAlignment = .right
            passwordTextField.textAlignment = .right
            nameTextField.textAlignment = .right
            confirmPasswordTextField.textAlignment = .right
        }
    }
    
    func closeKeypad() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func viewTapped() {
        nameTextField.endEditing(true)
        phoneTextField.endEditing(true)
        passwordTextField.endEditing(true)
        confirmPasswordTextField.endEditing(true)
    }
        
//    func presentRegistration() {
//        if let SignUpNavigationController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "navigationSignUpViewController") as? UINavigationController, let rootViewContoller = SignUpNavigationController.viewControllers[0] as? SignUpViewController {
//            //                rootViewContoller.test = "test String"
//            self.present(SignUpNavigationController, animated: true, completion: nil)
//        }
//    }
    
    //MARK:- Navigation
    func goToLogIn() {
        if let signInNavigationController = storyboard?.instantiateViewController(withIdentifier: "LoginNavigationVC") as? UINavigationController, let loginViewController = signInNavigationController.viewControllers[0] as? LoginViewController {

            UIApplication.shared.keyWindow?.rootViewController = signInNavigationController
        }
    }

    func presentHomeScreen(isPushNotification: Bool = false) {
        //        if let _ = UserDefaultManager.shared.currentUser, let _ = UserDefaultManager.shared.authorization {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "HomeNavigationVC")
        appDelegate.window!.rootViewController = viewController
        appDelegate.window!.makeKeyAndVisible()
        //        }
    }
    
    func goToTermsAndConditions() {
        if let termsAndConditions = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AgreeOnTermsViewController") as? AgreeOnTermsViewController {
            //                rootViewContoller.test = "test String"
//            self.present(termsAndConditions, animated: true, completion: nil)
            navigationController?.pushViewController(termsAndConditions, animated: true)
            
            print("List Of Chats")
        }
    }
    
    func signUp() {
        
        guard let name: String = nameTextField.text, name.count > 0 else {
              let apiError = APIError()
            if "Lang".localized == "en" {
                apiError.message = "Please enter your name"
            } else if "Lang".localized == "ar" {
                apiError.message = "برجاء إدخال الإسم"
            }
              self.viewModel.error = apiError
              self.viewModel.state = .error
              return
          }
        
        if name.count < 50 {
        } else {
            let apiError = APIError()
            if "Lang".localized == "en" {
                apiError.message = "Your name is more than 50 letters"
            } else if "Lang".localized == "ar" {
                apiError.message = "الإسم يزيد عن ٥٠ حرف"
            }
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let phone: String = phoneTextField.text, phone.count == 10 else {
            let apiError = APIError()
            if "Lang".localized == "en" {
                apiError.message = "Please enter the phone correctly"
            } else if "Lang".localized == "ar" {
                apiError.message = "برجاء إدخال رقم الجوال بشكل صحيح"
            }
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
                                
        guard let password: String = passwordTextField.text, password.count > 5 else {
            let apiError = APIError()
            if "Lang".localized == "en" {
                apiError.message = "Please enter more than or equal 6 words as a password"
            } else if "Lang".localized == "ar" {
                apiError.message = "برجاء إدخال كلمة المرور أكثر من ٦ حروف"
            }
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let confirmPassword: String = confirmPasswordTextField.text, confirmPassword == password else {
            let apiError = APIError()
            if "Lang".localized == "en" {
                apiError.message = "Please enter the password correctly"
            } else if "Lang".localized == "ar" {
                apiError.message = "برجاء إدخال تأكيد كلمة المرور بشكل صحيح"
            }
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        self.viewModel.signUp(name: name, password: password, phone: phone, cityID: 1)

    }
    
    //MARK:- Navigation
//    func goToVerifyPhone(id: Int) {
////        if let verifyPhoneVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VerifyPhoneNavigationVC") as? VerifyPhoneViewController {
////               verifyPhoneVC.id = id
////            self.present(verifyPhoneVC, animated: true, completion: nil)
////        }
//        if let verifyPhoneNavVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VerifyPhoneNavigationVC") as? UINavigationController, let rootViewContoller = verifyPhoneNavVC.viewControllers[0] as? VerifyPhoneViewController {
//                rootViewContoller.id = id
//            self.present(verifyPhoneNavVC, animated: true, completion: nil)
//        }
//    }
    
        
    @IBAction func registrationIsPressed(_ sender: Any) {
        print("Registration is pressed")
//        presentHomeScreen()
        self.signUp()
    }

}

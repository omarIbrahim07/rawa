//
//  StaticAPIManager.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/18/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class StaticAPIManager: BaseAPIManager {

    func getStaticPage(basicDictionary params:APIParams , onSuccess: @escaping (StaticPage)->Void, onFailure: @escaping  (APIError)->Void) {
    
    let engagementRouter = BaseRouter(method: .get, path: GET_STATIC_PAGE_URL, parameters: params)
    
    super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
        
        if let response: [String : Any] = responseObject as? [String : Any], let json: [String : Any] = response["data"] as? [String : Any], let wrapper = Mapper<StaticPage>().map(JSON: json) {
            onSuccess(wrapper)
        }
            
        else {
            let apiError = APIError()
            onFailure(apiError)
        }
        
        }) { (apiError) in
            onFailure(apiError)
        }
    }

}

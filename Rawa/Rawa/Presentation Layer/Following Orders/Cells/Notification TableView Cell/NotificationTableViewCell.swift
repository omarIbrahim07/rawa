//
//  NotificationTableViewCell.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 12/1/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {
    
    var statusEn: [String] = ["Waiting for payment", "Waiting for delegate", "Ongoing", "Delivered"]
    var statusAr: [String] = [
        "بانتظار الدفع",
        "بانتظار المندوب",
        "جاري التوصيل",
        "تم التوصيل"
    ]

    @IBOutlet weak var masjidLabel: UILabel!
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var notificationImage: UIImageView!
    
    var photoListCellViewModel : FollowingOrderCellViewModel? {
        didSet {
            if let image: String = photoListCellViewModel?.verifyImage {
                let url = URL(string: ImageURLOrders + image)
                notificationImage.kf.setImage(with: url)
            }
            if let mosqueName: String = photoListCellViewModel?.mosqueName {
                masjidLabel.text = mosqueName
            }
            
            if let date: String = photoListCellViewModel?.updatedAt {
                dateLabel.text = date
            }
            if let status: String = photoListCellViewModel?.status {
                let sta = Int(status)
                if "Lang".localized == "ar" {
                    stateLabel.text = statusAr[sta!]
                } else if "Lang".localized == "en" {
                    stateLabel.text = statusEn[sta!]
                }
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureView()
    }
    
    func configureView() {
//        containerView.layer.cornerRadius = 8.0
        containerView.addCornerRadius(raduis: 8.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

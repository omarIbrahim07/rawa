//
//  UIButton+Extensions.swift
//  GameOn
//
//  Created by Hassan on 12/16/17.
//  Copyright © 2017 Hassan. All rights reserved.
//

import UIKit

extension UIButton {
    
    func setEnable() {
        self.isEnabled = true
        self.alpha = 1.0
    }
    
    func setDisable() {
        self.isEnabled = false
        self.alpha = 0.66
    }
    
    func setDisable2() {
        self.isEnabled = false
        self.backgroundColor = UIColor().hexStringToUIColor(hex: "#999999")
    }
    
    func setDisable3() {
        self.isEnabled = false
        self.backgroundColor = UIColor().hexStringToUIColor(hex: "#96A5B4")
    }
    
    func setEnable2() {
        self.isEnabled = true
        self.backgroundColor = UIColor().hexStringToUIColor(hex: "#2F87DE")
    }
    
    
}

//
//  PlaceMarker.swift
//  Rawa
//
//  Created by Omar Ibrahim on 4/13/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class PlaceMarker: GMSMarker {
    
    let place: GooglePlace
    
    // 2
    init(place: GooglePlace) {
      self.place = place
      super.init()
      
      position = place.coordinate
      icon = UIImage(named: place.placeType+"_pin")
      groundAnchor = CGPoint(x: 0.5, y: 1)
      appearAnimation = .pop
    }

}

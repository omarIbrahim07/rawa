//
//  HomeViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/11/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation
import Toast_Swift

public enum State {
    case loading
    case error
    case populated
    case empty
}

class HomeViewModel {
    
    var selectedService: Service?
    
    private var services: [Service] = [Service]()
    var error: APIError?
    
    var cellViewModels: [HomeCellViewModel] = [HomeCellViewModel]() {
           didSet {
               self.reloadTableViewClosure?()
           }
       }
    
    var reloadTableViewClosure: (()->())?
    var showAlertClosure: (()->())?
    var updateLoadingStatus: (()->())?
    var updateSelectedOffer: (()->())?
    var reloadOffersPagerView: (()->())?
    var reloadCitiesClosure: (()->())?
    var chooseCityClosure: (()->())?
    var reloadItemsClosure: (()->())?
    var chooseItemClosure: (()->())?
    var updateItem: (()->())?
    var updateSendingOrder: (()->())?
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    public enum sendingOrder {
        case success
        case failed
    }

    var numberOfCells: Int {
         return cellViewModels.count
     }
    
    var offers: [Offer] = [Offer]() {
        didSet {
            self.reloadOffersPagerView?()
        }
    }

    var selectedOffer: Offer? {
        didSet {
            self.updateSelectedOffer?()
        }
    }
    
    var sendingOrder: sendingOrder = .failed {
        didSet {
            self.updateSendingOrder?()
        }
    }
    
    // callback for interfaces
    var cities: [City]? {
        didSet {
            self.reloadCitiesClosure?()
        }
    }
    
    // callback for interfaces
    var choosedCity: City? {
        didSet {
            self.chooseCityClosure?()
        }
    }
    
    var items: [Item]? {
        didSet {
            self.reloadItemsClosure?()
        }
    }
    
    // callback for interfaces
    var choosedItem: Item? {
        didSet {
            self.chooseItemClosure?()
        }
    }

    var item: ItemViewModel? {
        didSet {
            updateItem?()
        }
    }
    
//    var offerCellViewModels: [OfferCellViewModel] = [OfferCellViewModel]() {
//        didSet {
//            self.reloadOffersPagerView?()
//        }
//    }
    
    func initFetch() {
        state = .loading
        
        let params: [String : AnyObject] = [
            :
        ]
        
        CitiesAPIManager().getCities(basicDictionary: params, onSuccess: { (cities) in

        self.cities = cities // Cache
        self.state = .populated

        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func fetchItems() {
        state = .loading
        
        let params: [String : AnyObject] = [
            :
        ]
        
        CitiesAPIManager().getItems(basicDictionary: params, onSuccess: { (items) in

        self.items = items // Cache
        self.state = .populated

        }) { (error) in
            self.error = error
            self.state = .error
        }
    }

    func sendOrder(userID: Int, itemId: Int, quantity: Int, mosqueId: Int, mosqueName: String, cityId: Int, orderType: Int, longitude: String, latitude: String) {
        
        var params: [String : AnyObject]?
        
        if orderType == 0 {
            params = [
                "user_id" : userID as AnyObject,
                "item_id" : itemId as AnyObject,
                "quantity" : quantity as AnyObject,
                "mosque_id" : mosqueId as AnyObject,
                "mosque_name" : mosqueName as AnyObject,
                "city_id" : cityId as AnyObject,
                "order_type" : orderType as AnyObject,
            ]
        } else if orderType == 1 {
            params = [
                "user_id" : userID as AnyObject,
                "item_id" : itemId as AnyObject,
                "quantity" : quantity as AnyObject,
                "mosque_name" : mosqueName as AnyObject,
                "city_id" : cityId as AnyObject,
                "order_type" : orderType as AnyObject,
                "longitude" : longitude as AnyObject,
                "latitude" : latitude as AnyObject
            ]
        }
        
        self.state = .loading
        
        OrderAPIManager().SendOrder(basicDictionary: params, onSuccess: { (saved) in
            if saved == true {
                print("Success")
                self.state = .populated
                self.sendingOrder = .success
            } else {
                self.state = .empty
                self.sendingOrder = .failed
            }
        }) { (error) in
            self.error = error
            self.state = .error
            self.sendingOrder = .failed
        }
    }

    func fetchOffers() {
        state = .loading
        
        let params: [String : AnyObject] = [:]
        
        OffersAPIManager().getOffers(basicDictionary: params, onSuccess: { (offers) in

        self.processOffers(offers: offers)
            
        self.state = .populated

        }) { (error) in
            self.error = error
            self.state = .error
        }
    }

    
    func createCellViewModel( service: Service ) -> HomeCellViewModel {

        //Wrap a description
        var descTextContainer: [String] = [String]()
        if let camera = service.name {
            descTextContainer.append(camera)
        }
                
        if let description = service.image {
            descTextContainer.append( description )
        }
        
        if "Lang".localized == "ar" {
            return HomeCellViewModel(titleText: service.name!, imageUrl: service.image!)
        } else if "Lang".localized == "en" {
            return HomeCellViewModel(titleText: service.nameEN!, imageUrl: service.image!)
        }
        
        return HomeCellViewModel(titleText: service.name!, imageUrl: service.image!)
    }
    
    func processFetchedPhoto( services: [Service] ) {
        self.services = services // Cache
        var vms = [HomeCellViewModel]()
        for service in services {
            vms.append( createCellViewModel(service: service) )
        }
        self.cellViewModels = vms
    }
    
    func processOffers( offers: [Offer] ) {
        self.offers = offers // Cache
    }
    
    func getOffer() -> Offer {
        return selectedOffer!
    }

    func offerPressed( at indexPath: IndexPath ) {
        let offer = self.offers[indexPath.row]

        self.selectedOffer = offer
    }

    
    var numberOfOffers: Int {
        return self.offers.count
     }
    
    func getCellViewModel( at indexPath: IndexPath ) -> HomeCellViewModel {
        return cellViewModels[indexPath.row]
    }
    
//    func userPressed( at indexPath: IndexPath ){
//           let service = self.services[indexPath.row]
//
//        self.selectedService = service
//    }
    
    func getCities() -> [City] {
        return cities!
    }
    
    func userPressed(city: City) {
        self.choosedCity = city
    }
    
    func getItems() -> [Item] {
        return items!
    }
    
    func itemPressed(item: Item) {
        self.choosedItem = item
    }
    
    func getError() -> APIError {
        return self.error!
    }
    
}

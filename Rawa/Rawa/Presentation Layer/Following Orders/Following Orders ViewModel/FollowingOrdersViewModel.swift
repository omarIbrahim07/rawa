//
//  FollowingOrdersViewModel.swift
//  Rawa
//
//  Created by Omar Ibrahim on 4/20/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

class FollowingOrdersViewModel {
    
    private var followingOrders: [Order] = [Order]()
    var selectedOrder: Order?
    
    var error: APIError?
    
    var cellViewModels: [FollowingOrderCellViewModel] = [FollowingOrderCellViewModel]() {
           didSet {
               self.reloadTableViewClosure?()
           }
       }
    
    var reloadTableViewClosure: (()->())?
    var updateLoadingStatus: (()->())?
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var numberOfCells: Int {
         return cellViewModels.count
     }
    
    func initFetch() {
        state = .loading
        
        OrderAPIManager().getOrders(basicDictionary: [:], onSuccess: { (followingOrders) in
            
            self.processFetchedPhoto(followingOrders: followingOrders)
            self.state = .populated

        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func processFetchedPhoto( followingOrders: [Order] ) {
          self.followingOrders = followingOrders // Cache
          var vms = [FollowingOrderCellViewModel]()
          for followingOrder in followingOrders {
              vms.append(createCellViewModel(followingOrder: followingOrder))
          }
          self.cellViewModels = vms
      }
    
    func createCellViewModel( followingOrder: Order ) -> FollowingOrderCellViewModel {
        
        return FollowingOrderCellViewModel(id: followingOrder.id, userId: followingOrder.userId, itemId: followingOrder.itemId, quantity: followingOrder.quantity, mosqueName: followingOrder.mosqueName, status: followingOrder.status, verifyImage: followingOrder.verifyImage, updatedAt: followingOrder.updatedAt)
    }
    
    func getError() -> APIError {
        return self.error!
    }
    
    func getCellViewModel( at indexPath: IndexPath ) -> FollowingOrderCellViewModel {
        return cellViewModels[indexPath.row]
    }
    
    func userPressed( at indexPath: IndexPath ){
        let followingOrder = self.followingOrders[indexPath.row]

        self.selectedOrder = followingOrder
    }

    
}

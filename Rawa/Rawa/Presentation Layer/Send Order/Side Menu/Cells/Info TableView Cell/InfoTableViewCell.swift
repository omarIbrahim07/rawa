//
//  InfoTableViewCell.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/12/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit

protocol profileImageTableViewCellDelegate {
    func profileImageButtonPressed()
}

class InfoTableViewCell: UITableViewCell {

    var delegate: profileImageTableViewCellDelegate?
    @IBOutlet weak var trailing: NSLayoutConstraint!
        
    //MARK:- Delegate Helpers
    func profileImageButtonPressed() {
        if let delegateValue = delegate {
            delegateValue.profileImageButtonPressed()
        }
    }

    @IBOutlet weak var userImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        trailing.constant = self.frame.width / 3 - 100
//        trailing.constant = 30
    }

    
}

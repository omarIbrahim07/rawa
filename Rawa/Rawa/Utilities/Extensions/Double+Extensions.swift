//
//  Double+Extensions.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 12/23/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation

extension Double {
    
    private var formatter: DateComponentsFormatter {
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.minute, .second]
        formatter.unitsStyle = .positional
        formatter.zeroFormattingBehavior = .pad
        return formatter
    }
    
    func secondsToString() -> String {
        return formatter.string(from: self) ?? ""
    }
    
}
